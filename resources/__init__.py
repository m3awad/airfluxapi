import inspect
from .base import BaseResource
import resources.v1


def get_module_classes(module):
	for member in inspect.getmembers(module, inspect.isclass):
		yield member


def get_version_resources(version):
	for name, member in get_module_classes(version):
		if issubclass(member, BaseResource) and name != 'BaseResource':
			yield member


class Version:
	def __init__(self, module):
		self.name = module.__name__.split('.')[1]
		self.resources = [member for member in get_version_resources(module)]
		self.__doc__ = module.__doc__

	def __str__(self):
		return "{name}\n{dox}".format(name=self.name, dox=self.__doc__)


class API:
	def __init__(self):
		self.versions = []

	def add_version(self, version):
		self.versions.append(Version(version))

API = API()
API.add_version(v1)
