"""
Documentation for V1 API
"""
import falcon, ujson
from .base import BaseResource
from data import store


# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class FlightPlanResource(BaseResource):
	"""
	Flight plan Endpoint, returns all flights objects along with the aircraft data, 
			this is intended to help airports identifying all flights taking off from them.
	input: optional, airport code name
	output: flight objects
	"""
	endpoint = 'flightplan'
	
	def on_get(self, req, resp):
		"""Handles GET requests"""
		airport = req.params.get('airport')
		resp.body = ujson.dumps(store.get_tasks(airport))


class OperationsPlanResource(BaseResource):
	"""
	Operations plan resource, returns all scheduled flights for a specific aircraft identified by registeration string,
		this API is intended to help captains to know their day ahead
	input: mandatory, aircraft registeration
	output: Instruction objects
	"""
	endpoint = 'operationsplan'

	def on_get(self, req, resp):
		"""Handles GET requests"""
		registeration = req.params.get('registeration')
		resp.body = ujson.dumps(store.get_instructions(registeration))
