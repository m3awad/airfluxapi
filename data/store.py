import csv
from collections import defaultdict


class Store:
	def __init__(self, workers_file, locations_file, instructions_file):
		self.workers_file = workers_file
		self.locations_file = locations_file
		self.instructions_file = instructions_file
		self.index_by_airport = defaultdict(list)
		self.index_by_registeration = defaultdict(list)
		self.tasks = []
		self.workers = {}
		self.airports = {}
		self.index()

	def index(self):
		with open(self.workers_file, 'r') as wf:
			wfcv = csv.DictReader(wf, delimiter=',', quotechar='|')
			for row in wfcv:
				row.pop('AVAILABILITY')
				row.pop('AirportID')
				_id = row.pop('ID')
				self.workers[_id] = row
		with open(self.locations_file, 'r') as lf:
			lfcv = csv.DictReader(lf, delimiter=',', quotechar='|')
			for row in lfcv:
				self.airports[row['ID']] = row
		with open(self.instructions_file, 'r') as inf:
			infcv = csv.DictReader(inf, delimiter=',', quotechar='|')
			for row in infcv:
				start, end, worker = (self.airports[row['Start']]['CODE'], 
										self.airports[row['End']]['CODE'], 
										self.workers[row['WorkerID']])
				row['departure_airport'] = start
				row['arrival_airport'] = end
				row['aircraft'] = worker
				row.pop('Start')
				row.pop('End')
				row.pop('WorkerID')
				row['duration'] = int(row.pop('Cost', 0))
				row['Time'] = int(row['Time'])
				row['ETA'] = int(row['ETA'])
				self.index_by_airport[start].append(row)
				self.index_by_registeration[worker['REGISTERATION']].append(row)
				self.tasks.append(row)

	def get_tasks(self, airport=None):
		if not airport:
			return sorted(self.tasks, key=lambda x: x['Time'])
		return sorted(self.index_by_airport.get(airport, []), key=lambda x: x['Time'])

	def get_instructions(self, registeration):
		return sorted(self.index_by_registeration.get(registeration, []), key=lambda x: x['Time'])


store = Store('batch/workers.csv', 'batch/locations.csv', 'batch/instructions.csv')
