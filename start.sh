echo "Installing requirements"
pip install -r requirements.txt
echo "scheduling events"
python batch/management.py
echo "running app"
gunicorn server:app