"""
This is the start point of the falcon server
Here we declare the falcon app object and define the routes from resources
We also add the Home endpoint
"""
import inspect
from collections import defaultdict
import falcon, ujson, io
from resources import API
import subprocess


app = falcon.API()
resources = defaultdict(list)
WORKERS = 'batch/workers.csv'
INSTRUCTIONS = 'batch/instructions.csv'
AIRPORTS = 'batch/locations.csv'

# First import the endpoints from the resources module and add their documentation to the home API
for version in API.versions:
	version_name = version.name
	for resource in version.resources:
		falcon_resource = resource()
		endpoint = '/{version}/{endpoint}'.format(version=version_name, endpoint=falcon_resource.endpoint)
		resources[version_name].append({'endpoint': endpoint, 'description': resource.__doc__})
		app.add_route(endpoint, falcon_resource)


resources = ujson.dumps(resources)
class HomeResource:	
	def on_get(self, req, resp):
		"""Handles GET requests"""
		resp.body = resources


app.add_route('/', HomeResource())
