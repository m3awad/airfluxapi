from pyswip import Prolog
import ujson
import sys, contextlib, csv
from io import StringIO


airports_cities = {
	'london': ('lhr', 0),
	'munich': ('muc', 1),
	'berlin': ('txl', 2),
	'hamburg': ('ham', 3)
}
airport_ids_by_code = {v[0]: v[1] for v in airports_cities.values()}
airport_code_by_ids = {v[1]: v[0] for v in airports_cities.values()}


class Worker:
	def __init__(self, airport_id, name, equipment, registeration):
		self.name = name
		self.equipment = equipment
		self.registeration = registeration
		self.id = None
		self.airport_id = airport_id
		self.availability = 0

	@property
	def fact(self):
		return 'worker(%d, %d, %d).' %(self.id, self.airport_id, self.availability)

	def as_row(self):
		return [self.id, self.airport_id, self.availability, self.name, self.equipment, self.registeration]


class Location:
	def __init__(self, _id):
		self._id = _id
		self.code_name = airport_code_by_ids[_id]

	@property
	def fact(self):
		return 'location(%d).' %self._id

	def as_row(self):
		return [self._id, self.code_name]
	


class Task:
	def __init__(self, src, dest, dep, time):
		self.src = src
		self.dest = dest
		self.dep = dep
		self.time = time

	@property
	def fact(self):
		return 'task(%d,%d,%d,%d)' %(self.src,self.dest,self.dep,self.time)

	def as_row(self):
		return [self.src,self.dest,self.dep,self.time]
	

class Constraints:
	def __init__(self):
		self.tasks = []

	def append(self, task):
		self.tasks.append(task)

	def dump(self):
		with open('tasks.csv', 'w') as tf:
			tcsv = csv.writer(tf, delimiter=',', quotechar='|')
			tcsv.writerow(['SRC','DEST', 'DEP', 'TIME'])
			for t in self.tasks:
				tcsv.writerow(t.as_row())

	def __iter__(self):
		for t in sorted(self.tasks, key=lambda x: x.dep):
			yield t

	def __len__(self):
		return len(self.tasks)

	@property
	def facts_pl(self):
		return ','.join([i.fact for i in self])


class Facts:
	def __init__(self):
		self.workers = []
		self.locations = []
		self.max_worker_id = 0

	def append_worker(self, worker):
		worker.id = self.max_worker_id
		self.workers.append(worker)
		self.max_worker_id += 1

	def append_loc(self, loc):
		self.locations.append(loc)

	def dump(self):
		with open('batch/workers.csv', 'w') as wf:
			wcsv = csv.writer(wf, delimiter=',', quotechar='|')
			wcsv.writerow(['ID', 'AirportID', 'AVAILABILITY', 'NAME', 'EQUIPMENT', 'REGISTERATION'])
			for worker in self.workers:
				wcsv.writerow(worker.as_row())
		with open('batch/locations.csv', 'w') as lf:
			lcsv = csv.writer(lf, delimiter=',', quotechar='|')
			lcsv.writerow(['ID', 'CODE'])
			for loc in self.locations:
				lcsv.writerow(loc.as_row())


	@property
	def all(self):
		for w in self.workers:
			yield w
		for l in self.locations:
			yield l

	@property
	def facts_pl(self):
		return '\n'.join([i.fact for i in self.workers]), '\n'.join([i.fact for i in self.locations])


class Event:
	def __init__(self, worker, src, dest, time, cost, eta):
		self.worker = worker
		self.src = src
		self.dest = dest
		self.time = time
		self.eta = eta
		self.cost = cost

	def as_row(self):
		return [self.worker, self.src, self.dest, self.time, self.cost, self.eta]


def parse_aircrafts_file(facts):
	with open('batch/aircrafts.txt', 'r') as workersf:
		for line in workersf.readlines():
			aircraft, info = line.split(' – ')
			base, info = info.split(', ')
			name, equipment = aircraft.split(' ')
			registeration = info.strip('registeration ').rstrip()
			airport_id = airports_cities[base.lower()][1]
			facts.append_worker(Worker(airport_id, name, equipment, registeration))

def parse_schedule_file(facts, constraints):
	location_facts = []
	task_facts = []
	with open('batch/schedule.txt', 'r') as schedulef:
		new = True
		sep = False
		task = False
		airport_id = 0
		for line in schedulef.readlines():
			line = line.rstrip().lower()
			if not line:
				task = False
				new = True
				continue
			if new:
				code = line
				# lcsv.writerow(['ID', 'CITY', 'NAME', 'CODE'])
				facts.append_loc(Location(airport_id, ))
				airport_id += 1
				new = False
				sep = True
				continue
			if sep:
				sep = False
				task = True
				continue
			if task:
				dep, src, dest, time = line.split(' ')
				dep = int(dep.replace(':', ''))
				hrs, mins = time.split(':')
				time = int(hrs)*60 + int(mins)				
				src = airport_ids_by_code[src.lower()]
				dest = airport_ids_by_code[dest.lower()]
				constraints.append(Task(src, dest, dep, time))
				continue


def write_pl(pl_file, facts, constraints):
	with open('batch/pl_template', 'r') as pl_template:
		code = pl_template.read()
		workers, locations = facts.facts_pl
		tasks = constraints.facts_pl
		code = code.format(workers=workers, locations=locations, tasks=tasks)
		# code = code.replace('$', '\\"').replace('->', '{').replace('<-', '}')
		with open(pl_file , 'w') as pl_code:
			pl_code.write(code)


def make_pl(opcode):
	facts = Facts()
	constraints = Constraints()
	parse_schedule_file(facts, constraints)
	parse_aircrafts_file(facts)
	pl_file = 'batch/%s.pl' %opcode
	write_pl(pl_file, facts, constraints)
	return pl_file, facts, constraints	


class Schedule:
	def __init__(self, opcode, prolog, pl_file, facts, constraints):
		self.prolog = prolog
		self.output = None
		self.facts = facts
		self.events = []
		self.constraints = constraints
		self.pl_file = pl_file

	def schedule(self):
		self.prolog.consult(self.pl_file)
		q = list(self.prolog.query("schedule(Events)."))
		self.output = list(self.prolog.query("events_length(L)."))
		L = self.output[0]['L']
		if not L>=len(self.constraints):
			print('failed with current fleet adding another aircraft')
			tasks = list(self.constraints)
			task = tasks[L-1]
			worker = Worker(task.src, 'made_up_name', 'made_up_equipment', 'made_up_registeration')
			worker.availability = task.dep
			self.facts.append_worker(worker)
			write_pl(self.pl_file, self.facts, self.constraints)
			self.schedule()
		else:
			for event in q[0].get('Events', [])[1:]:
				# print(event)
				self.events.append(Event(*event))

	def dump(self):
		self.facts.dump()
		self.constraints.dump()
		with open('batch/instructions.csv', 'w') as inf:
			incsv = csv.writer(inf, delimiter=',', quotechar='|')
			incsv.writerow(['WorkerID','Start','End','Time','Cost','ETA'])
			for e in self.events:
				incsv.writerow(e.as_row())

def schedule(opcode):
	pl_file, facts, constraints = make_pl(opcode)
	prolog = Prolog()
	schedule = Schedule(opcode, prolog, pl_file, facts, constraints)
	schedule.schedule()
	schedule.dump()
	return schedule


schedule('catherina')
