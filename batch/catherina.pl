use_module(library(clpfd)).
:-dynamic(worker/3).


worker(0, 2, 0).
worker(1, 1, 0).
worker(2, 0, 0).
worker(3, 3, 0).
worker(4, 0, 1700).


location(0).
location(1).
location(2).
location(3).

task(_, _, _, _):- true.
event(_, _, _, _, _, _):- true.


eta(Start,Duration, Y):- 
	X is Duration // 60, 
	Y is Start + (100 * X) + mod(Duration, 60).


find_available(Airport, Time, Worker):-
	findnsols(1, ID, (worker(ID,Airport,D), D=<Time), [WorkerID]),
	Worker = worker(WorkerID, Airport, _).

travel(task(Start, End, Time, Cost)) :-
	find_available(Start, Time, worker(WorkerID, LastAirport, _)),
	X = worker(WorkerID, LastAirport, _),
	retract(X),
	eta(Time, Cost, ETA),
	assert(worker(WorkerID, End, ETA)),
	assert(event(WorkerID, Start, End, Time, Cost, ETA)).

travel_all([]).
travel_all([SomeTask|Rest]):-
	once(travel(SomeTask)),
	travel_all(Rest).
travel_all():-
	travel_all([task(0,3,900,150),task(2,1,1000,60),task(1,0,1000,120),task(3,1,1000,60),task(0,2,1200,120),task(1,2,1300,60),task(3,1,1300,60),task(2,1,1500,60),task(1,2,1500,60),task(1,0,1530,120),task(2,1,1600,60),task(0,2,1700,120),task(1,3,1730,60),task(2,1,1800,60),task(1,0,1800,150),task(1,0,2000,120),task(3,1,2000,60),task(0,1,2030,120),task(2,3,2100,40),task(1,2,2200,60)]).


schedule(Events):-
	travel_all(),
	setof([A,B,C,D,E,F], event(A,B,C,D,E,F), Events).


events_length(L):-
	findall(_, event(_,_,_,_,_,_), Events), length(Events, L).
