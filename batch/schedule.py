import io, subprocess, csv


def schedule_flights():
	f = io.StringIO()
	p = subprocess.Popen(["python", "batch/management.py"], stdout=subprocess.PIPE)
	out, err = p.communicate()
	lines = str(out).split('\\n')
	last = max([i for i, x in enumerate(lines) if x == "BEGIN HEADER"])
	with open('batch/instructions.csv', 'w') as inf:
		tcsv = csv.writer(inf, delimiter=',', quotechar='|')
		tcsv.writerow(['WorkerID','Start', 'End', 'Time', 'Cost', 'ETA'])
		for line in lines[last:]:
			if line:
				if ',' in line:
					line = line.split(',')
					tcsv.writerow(line)

schedule_flights()
